#include <stdlib.h>
#include <opencv2/imgproc/imgproc.hpp>
#include "mt-functions.h"
#include <math.h>


/* **** Mini-�lohy - sada #2 ************************************* */

/*
	Povolen� metody a funkce OpenCV: 
		Mat: rows, cols, step(), size(), at<>(), zeros(), ones(), eye()
		d�le: saturate_cast<>()
*/

/*  Funkce pro �edot�nov� obraz (0-255) spo��t� konvoluci se zadan�m j�drem o velikost 3x3 typu float (CV_32FC1).
    Krajn� hodnoty v�sledn�ho obrazu ponechte 0.
	Implementujte ru�n� pr�chodem obrazem, pro ka�d� pixel projd�te jeho okol� a prove�te konvoluci s j�drem.

*/
void convolution( Mat& gray, const Mat& kernel, Mat& dst )
{
	dst = Mat::zeros(gray.size(), CV_8UC1 );

	if( kernel.rows != 3 || kernel.cols != 3 )
		return;

	// krajn� hodnoty obrazu nech�me 0
	for( int j = 1; j < gray.rows-1; ++j ) {
		for( int i = 1; i < gray.cols-1; ++i ) {

			float sum = 0;

            for( int k = 0; k < 3; ++k)
            {
                for (int l = 0; l < 3; ++l)
                {
                    sum += kernel.at<float>(k,l) * saturate_cast<float>(gray.at<uchar>(j + k - 1,i + l - 1));
                }
            }
            //save normalized output
            dst.at<uchar>(j,i) = saturate_cast<uchar>(sum / 9.);

		}
	}

}


Mat invertTransformationMat( const Mat& T )
{
	double M[6];
    Mat matM(2, 3, CV_64F, M); 
	T.convertTo(matM, matM.type()); 

    double D = M[0]*M[4] - M[1]*M[3];		
    D = D != 0 ? 1./D : 0;
    double A11 = M[4]*D, A22=M[0]*D;
    M[0] = A11; M[1] *= -D;
    M[3] *= -D; M[4] = A22;
    double b1 = -M[0]*M[2] - M[1]*M[5];
    double b2 = -M[3]*M[2] - M[4]*M[5];
    M[2] = b1; M[5] = b2;

	Mat out;
	matM.convertTo(out,T.type());
	return out;
}

/*  Funkce provede geometrickou transformaci obrazu s vyu�it�m interpolace nejbli���m sousedem.
*/
void geometricalTransform( const Mat& src, Mat& dst, const Mat& transformation )
{
	Mat T = invertTransformationMat( transformation );

	dst = Mat::zeros(src.size(), CV_8UC1 );

    // warpAffine(src, dst, T, src.size());

    for (int i = 0; i < dst.rows; ++i) // Y
    {
        for (int j = 0; j < dst.cols; ++j) // X
        {// T.at<float>(i,j)
            // vypoctu si koeficienty
            float x = T.at<float>(0,0) * j + T.at<float>(0,1) * i + T.at<float>(0,2);
            float y = T.at<float>(1,0) * j + T.at<float>(1,1) * i + T.at<float>(1,2);

            // check the bounds
            if((x < 0 || x > dst.cols) || (y < 0 || y > dst.rows))
                continue;
            
            // apply fix to improove results against CV functions        
            x += 0.00055555555;
            y += 0.00055555555;

            dst.at<uchar>(i,j) = src.at<uchar>(saturate_cast<int>(y),saturate_cast<int>(x));
        }
    }

	return;
}


/* *************************************************************** */

