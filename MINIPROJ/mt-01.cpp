#include <stdlib.h>
#include <opencv2/imgproc/imgproc.hpp>
#include "mt-functions.h"
#include <math.h>

#include <iostream>


using namespace std;
/*
	Implementujte vlastnoru�n� funkci, kter� p�evede barevn� 3-kan�lov� obr�z ve form�tu BGR do jednokan�lov�ho �edot�nov�ho obrazu. 
	G = 0.299*R + 0.587*G + 0.114*B;

	Upozorn�n�: ...
	Note that the default color format in OpenCV is often referred to as RGB but it is actually BGR (the bytes are reversed). 
	So the first byte in a standard (24-bit) color image will be an 8-bit Blue component, the second byte will be Green, and 
	the third byte will be Red.

	Realizujte pomoc� ukazatel� p��mo do pam�ti: unsigned char * ptr.
	Ukazatel na data obr�zk� je: Mat.data.
	Pozor, ���ka ��dku nemus� z d�vodu zarovn�n� v pam�ti odpov�dat: po�et_kan�l�*���ka_matice 
		- tedy ne: Mat.cols*Mat.channels()
		- ale vyu��t atribut: Mat.step

*/
void bgr2gray( const Mat& bgr, Mat& gray )
{
	gray = Mat::zeros(bgr.rows, bgr.cols, CV_8UC1 );

	uchar *p = bgr.data;

	size_t rows = bgr.rows;
	size_t cols = bgr.cols;

	for (size_t i = 0; i < rows; ++i)
	{
		for (size_t j = 0; j < cols; ++j)
		{
			double b = p[bgr.step * i + j * bgr.elemSize()]; // points to data
			double g = p[bgr.step * i + j * bgr.elemSize() + 1]; // points to data
			double r = p[bgr.step * i + j * bgr.elemSize() + 2]; // points to data

			double tGray = 0.299*r + 0.587*g + 0.114*b;
			gray.data[gray.step * i + j * gray.elemSize()] = floor(tGray + 0.5);

		}
	}

}



/*  Funkce spo��t� histogram �edot�nov�ho obrazu (0-255) a pomoc� ekvalizace histogramu zlep�� jeho kontrast.
    M��ete vyu��t funkci pro p��stup k hodnot�m matice: Mat.at<>
	P��klad: hodnotu jasu pixelu na ��dku 100 a sloupci 200 (tedy x=200, y=100) �edot�nov�ho obrazu z�sk�me:
		unsigned char pixel_value = obraz.at<unsigned chat>(100,200);
	
	D�le se hod� funkce saturate_cast, kter� prov�d� efektivn� a p�esn� p�evod mezi typy primitiv.
	P��klad sn�en� kontrastu na polovic a sn�en� intensity jasu cel�ho obrazu o 127 pak m��e pro jeden pixel vypadat takto:
		obraz.at<unsigned chat>(100,200) = saturate_cast<unsigned char>( 0.5 * obraz.at<unsigned chat>(100,200) - 127.0 );
	
	Informace o ekvalizaci histogramu, nap�.:
		http://docs.opencv.org/doc/tutorials/imgproc/histograms/histogram_equalization/histogram_equalization.html

*/
void histogramEqualization( const Mat& gray, Mat& eq_gray )
{
	Mat h   = Mat::zeros( 1, 256, CV_32SC1 );
	eq_gray = Mat::zeros(gray.size(), CV_8UC1 );

	size_t rows = gray.rows;
	size_t cols = gray.cols;
	// projd�te obraz a spo�t�te histogram obrazu
	// ...
	for (size_t i = 0; i < rows; ++i)
	{
		for (size_t j = 0; j < cols; ++j)
		{
			int val = gray.at<uchar>(i,j); // points to data

			h.at<float>(val)++;
		}
	}
 	rows = h.rows;
 	cols = h.cols;

 	int total = gray.rows * gray.cols;

	for (size_t i = 0; i < rows; ++i)
	{
		for (size_t j = 0; j < cols; ++j)
		{
			h.at<float>(i,j) /= total; //@TODO check that /=
		}
	}

	// spo�t�te akumulovan� histogram
	// hodnota v akumulovan�m histogramu pro danou intensitu je rovna sou�tu v�ech hodnot histogramu s ni���mi intenzitami.
	// ...
	float sum = 0;
	for (size_t i = 0; i < rows; ++i)
	{
		for (size_t j = 0; j < cols; ++j)
		{
			sum += h.at<float>(i,j);
			h.at<float>(i,j) = sum;

			// printf("%f\n", sum );
		}
	}	


	// p�epo�ti hodnoty jas� v obraze
	// double coef = 255./(gray.cols*gray.rows);
	// ...

	rows = eq_gray.rows;
	cols = eq_gray.cols;
	// projd�te obraz a spo�t�te histogram obrazu
	// ...
	for (size_t i = 0; i < rows; ++i)
	{
		for (size_t j = 0; j < cols; ++j)
		{
			eq_gray.at<uchar>(i,j) = floor(h.at<float>(gray.at<uchar>(i,j)) * 255 + .5 ); // points to data
		}
	}


	// return;
}