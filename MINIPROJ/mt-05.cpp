#include <stdlib.h>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <algorithm>    
#include <vector>       
#include "mt-functions.h"
#include <math.h>


/* **** Mini-�lohy - sada #5 ************************************* */
/*  Funkce pro generov�n� �umu typu pep� a s�l.
    Po�et ud�v�, kolik pixel� v obraze bude �umem zkresleno.
*/
void peprAsul( const Mat& src, Mat& dst, int pocet )
{
	dst = src.clone();
	cv::RNG rng;
	for( int i = 0; i < pocet; ++i ) {
		dst.at<int>(rng.uniform(0,dst.rows),rng.uniform(0,dst.cols)) = ((rng.uniform(0,255)) >= 127) ? 255 : 0;
		// dst.at<int>(rand() % src.rows,rand() % src.cols) = ((rand() % 1 + 1) == 0) ? 0 : 255;
		// printf("%d\n", (rand() % 255 > 127) ? 1 : 0 );		
	}
}


/*  Funkce pro filtraci obrazu pomoc� medi�nov�ho filtru.
     - velikost filtru v pixelech

	N�sleduj�c� �e�en� je p�edev��m ilustra�n�. B�n� se pro v�po�et medi�nu pou��vaj� optimalizavan� p��stupy �azen�.
	http://ndevilla.free.fr/median/median/index.html 
*/
void median( const Mat& src, Mat& dst, int velikost )
{
	// velikost - mus� b�t lich� ��slo, minim�ln� 3
	int stred = velikost/2;
	stred = MAX(1,stred);
	velikost = 2*stred+1;
	
	// zv�t��me obraz a okop�rujeme krajn� hodnoty do okraj�
	Mat srcBorder;
	copyMakeBorder( src, srcBorder, stred, stred, stred, stred, BORDER_REPLICATE );

	// p�iprav�me v�stupn� obraz
	dst = Mat( src.size(), src.type() );

	// p�iprav�me vektor k �azen� prvk�
	vector<uchar> buff(velikost*velikost);

	printf("\n%s\n", "================================================");
	// printf("%d\n", buff.size());
	// fflush(stdout);
	// return;
	Mat m(buff);
	m=m.reshape(1, velikost);

	for( int j = stred; j < dst.rows + stred; ++j ) {
		for( int i = stred; i < dst.cols + stred; ++i ) {
			// projdu cely buffer a nahazu tam odpovidajici prvky
			
			for (int k = 0; k < velikost; ++k)
			{
				for (int l = 0; l < velikost; ++l)
				{
					m.at<uchar>(k,l) = srcBorder.at<uchar>(j - (stred - k), i - (stred - l));
				}
			}
			std::sort(buff.begin(), buff.end());
			
			dst.at<uchar>(j-stred,i-stred) = buff[velikost*velikost/2];
		}
	}

	return;
}
