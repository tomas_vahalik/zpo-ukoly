#include <stdlib.h>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;


//#define MT01
//#define MT02
// #define MT03
// #define MT04
#define MT05

/*
    V�ce informac� na str�nk�ch p�edm�tu ZPO.
*/

/* **** Mini-�lohy - sada #1 ************************************* */
#ifdef MT01
/*  Funkce p�evede barevn� 3-kan�lov� obr�z ve form�tu BGR do jednokan�lov�ho �edot�nov�ho obrazu. 
*/
void bgr2gray( const Mat& bgr, Mat& gray );

/*  Funkce spo��t� histogram �edot�nov�ho obrazu (0-255) a pomoc� ekvalizace histogramu zlep�� jeho kontrast.
*/
void histogramEqualization( const Mat& gray, Mat& eq_gray );
#endif
/* *************************************************************** */


/* **** Mini-�lohy - sada #2 ************************************* */
#ifdef MT02
/*  Funkce pro �edot�nov� obraz (0-255) spo��t� konvoluci se zadan�m j�drem o velikost 3x3.
*/
void convolution( Mat& gray, const Mat& kernel, Mat& dst );

/*  Funkce provede geometrickou transformaci obrazu s vyu�it�m interpolace nejbli���m sousedem.
*/
void geometricalTransform( const Mat& src, Mat& dst, const Mat& T );
#endif
/* *************************************************************** */



/* **** Mini-�lohy - sada #3 ************************************* */
#ifdef MT03
/*  Funkce pro filtraci obrazu - doln�, resp. horn� propust.
*/
#define LOW_PASS_FILTER 1
#define HIGH_PASS_FILTER 2
void passFilter( const Mat& src, Mat& dst, int limit_frequency, int flag );
#endif
/* *************************************************************** */



/* **** Mini-�lohy - sada #4 ************************************* */
#ifdef MT04
/*  Funkce pro generov�n� b�l�ho �umu.
    Intenzita ud�v� maxim�ln� velikost �umu.
*/
void bilySum( const Mat& src, Mat& dst, float intenzita );

/*  Funkce pro filtraci obrazu Gaussov�m filtrem.
     - velikost filtru v pixelech
     - sigma je rozptyl pro v�po�et hodnot Gaussovy funkce
*/
void separabilityGauss( const Mat& src, int velikost, double sigma, Mat& sepDst, Mat& noSepDst );
#endif
/* *************************************************************** */



/* **** Mini-�lohy - sada #5 ************************************* */
#ifdef MT05
/*  Funkce pro generov�n� �umu typu pep� a s�l.
    Po�et ud�v�, kolik pixel� v obraze bude �umem zkresleno.
*/
void peprAsul( const Mat& src, Mat& dst, int pocet );

/*  Funkce pro filtraci obrazu pomoc� medi�nov�ho filteru.
     - velikost filtru v pixelech
    N�sleduj�c� �e�en� je p�edev��m ilustra�n�. B�n� se pro v�po�et medi�nu pou��vaj� optimalizavan� p��stupy �azen�.
    http://ndevilla.free.fr/median/median/index.html 

*/
void median( const Mat& src, Mat& dst, int velikost );
#endif
/* *************************************************************** */
