#include <stdlib.h>
#include <opencv2/imgproc/imgproc.hpp>
#include "mt-functions.h"
#include <math.h>
#include <opencv2/opencv.hpp>

void rearrangeSpectrum( Mat& s )
{
   // rearrange the quadrants of Fourier image  so that the origin is at the image center
    int cx = s.cols/2;
    int cy = s.rows/2;

    Mat q0(s, Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
    Mat q1(s, Rect(cx, 0, cx, cy));  // Top-Right
    Mat q2(s, Rect(0, cy, cx, cy));  // Bottom-Left
    Mat q3(s, Rect(cx, cy, cx, cy)); // Bottom-Right

    Mat tmp;                           // swap quadrants (Top-Left with Bottom-Right)
    q0.copyTo(tmp);
    q3.copyTo(q0);
    tmp.copyTo(q3);

    q1.copyTo(tmp);                    // swap quadrant (Top-Right with Bottom-Left)
    q2.copyTo(q1);
    tmp.copyTo(q2);
}

Mat spectrumMagnitude( Mat & specCplx )
{
	Mat specMag, planes[2];
	split(specCplx, planes);						
	magnitude(planes[0], planes[1], planes[0]);		
	log( (planes[0] + Scalar::all(1)), specMag );
	normalize( specMag, specMag, 0, 1, CV_MINMAX );
	return specMag;
}

/* **** Mini-�lohy - sada #3 ************************************* */

/*  Funkce pro filtraci obrazu - doln�, resp. horn� propust.
*/
void passFilter( const Mat& src, Mat& dst, int limit_frequency, int flag )
{
	// najdeme optim�ln� velikost obrazu pro efektivn� v�po�et DFT
	Size dftSize;
	dftSize.width = getOptimalDFTSize(src.cols);
	dftSize.height = getOptimalDFTSize(src.rows);

	// p�iprav�me nov� obraz optim�ln� velikosti - hodnoty mimo zdrojov� obraz nastav�me na 0
	Mat srcPadded, srcCplx;
    copyMakeBorder( src, srcPadded, 0, dftSize.height-src.rows, 0, dftSize.width-src.cols, BORDER_CONSTANT, Scalar::all(0));
	// p�iprav�me komplexn� reprezentaci zdrojov�ho obrazu
	Mat planes[] = {Mat_<float>(srcPadded), Mat::zeros(srcPadded.size(), CV_32F)};
	merge(planes, 2, srcCplx);         

	// vyu�ijte funkci dft knihovny OpenCV pro v�po�et spektra
	Mat specCplx;
	dft(srcCplx, specCplx, 0, src.rows );  

	// prohozen� kvadrant� spektra
	rearrangeSpectrum( specCplx );

	Mat mask;
	// p�iprav�me masku na vymaz�n� ��st� spektra
	int lim_freq = MIN( limit_frequency, MIN(specCplx.rows,specCplx.cols ) );
	Rect band( (specCplx.cols-lim_freq)/2, (specCplx.rows-lim_freq)/2, lim_freq, lim_freq );

	if( flag == HIGH_PASS_FILTER ) {
		// ponech�me pouze hodnoty spektra nad limitn� frekvenc�
        for (int y = band.y; y < band.y + band.height; ++y)
        {
            for (int x = band.x; x < band.x + band.width; ++x)
            {
                specCplx.at<Vec2f>(y,x)[0] = 0;
                specCplx.at<Vec2f>(y,x)[1] = 0;
            }
        }
	}
	else if( flag == LOW_PASS_FILTER ) {
		// ponech�me pouze hodnoty spektra pod limitn� frekvenc�

        for (int y = 0; y < specCplx.rows; ++y)
        {
            for (int x = 0; x < specCplx.cols; ++x)
            {
                if( ((x <= band.x) || (x > (band.x + band.width))) || ((y <= band.y) || (y > (band.y + band.height))))
                {
                    specCplx.at<Vec2f>(y,x)[0] = 0;
                    specCplx.at<Vec2f>(y,x)[1] = 0;
                }

            }
        }

	}

	// prohozen� kvadrant� spektra zp�t
	rearrangeSpectrum( specCplx );

	// vyu�ijte funkci dft (nebo idft) knihovny OpenCV pro zp�tnou transformaci
	dft( specCplx, srcCplx, DFT_INVERSE+DFT_SCALE, src.rows );

	split(srcCplx, planes);  
	normalize( planes[0], planes[0], 0, 1, CV_MINMAX );
	planes[0](Rect(Point(0,0),src.size())).convertTo( dst, src.type(), 255, 0 );

	imshow("Puvodn� obraz", src );
	imshow("Velikost Spektra", spectrumMagnitude(specCplx) );
	imshow("Filtrovan� obraz", dst );
	waitKey();

	return;
}
