#include <stdlib.h>
#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include "mt-functions.h"
#include <math.h>


/* **** Mini-�lohy - sada #4 ************************************* */
/*  Funkce pro generov�n� b�l�ho �umu.
    Intenzita ud�v� maxim�ln� velikost �umu.
*/
void bilySum( const Mat& src, Mat& dst, float intenzita )
{
	// fprintf(stderr, "%s\n", "test");
	cv::RNG rng;
	Mat sum = Mat::zeros( src.size(), CV_32SC1 );
	// printf("rows-%d:cols-%d => %d\n", sum.rows, sum.cols, sum.rows * sum.cols );

	double num = sqrt(intenzita);

	for( int i = 0; i < sum.rows; ++i ) {
		for (int j = 0; j < sum.cols; ++j)
		{
			// sum.at<int>(i,j) = saturate_cast<int>(num * rng.uniform(0.,1.));
			sum.at<int>(i,j) = saturate_cast<int>(num * rng.gaussian(1.));
		}

	}
	Mat tmp;
	src.convertTo( tmp, sum.type() );
	tmp = tmp+sum;
	tmp.convertTo( dst, src.type() );

	// fprintf(stderr, "%s\n", "test2");
}


/*  Funkce pro filtraci obrazu Gaussov�m filtrem.
     - velikost filtru v pixelech
	 - sigma je rozptyl pro v�po�et hodnot Gaussovy funkce
*/
void separabilityGauss( const Mat& src, int velikost, double sigma, Mat& sepDst, Mat& noSepDst )
{
	// velikost - mus� b�t lich� ��slo, minim�ln� 3
	int stred = velikost/2;
	stred = MAX(1,stred);
	velikost = 2*stred+1;

	// p�ipravte Gauss�v filtr v 1D 
	Mat gauss1D = Mat::zeros( 1, velikost, CV_64FC1 );
	double a = 1./(sigma*sqrt(2*CV_PI));
	double b = 1./(2*sigma*sigma);
	for( int i = 0; i < velikost; ++i ) {
		double hodnota = 0.;
		hodnota = a*exp( -b* (i-(velikost-1)/2)*(i-(velikost-1)/2) );

		gauss1D.at<double>(i) = hodnota;
	}
	// normalizace hodnot
	gauss1D = gauss1D / sum(gauss1D).val[0];

	// p�ipravte Gauss�v filtr ve 2D 
	// vyu�ijte konvoluce 1D Gauss. j�dra ve sm�ru x a y s jednotkov�m impulsem
	Mat gauss2D = Mat::zeros( velikost, velikost, CV_64FC1 );
	gauss2D.at<double>(stred,stred) = 1.;
	filter2D( gauss2D, gauss2D, -1, gauss1D );
	filter2D( gauss2D, gauss2D, -1, gauss1D.t() );
	gauss2D = gauss2D / sum(gauss2D).val[0];

	// rozmaz�n� obrazu s vyu�it�m separability oper�toru - vyu��t 1D filtr
	sepDst = Mat::zeros(src.size(),src.type());
	sepFilter2D(src, sepDst,-1, gauss1D, gauss1D.t());


	// rozmaz�n� obrazu bez vyu�it� separability oper�toru - vyu��t 2D filtr
	noSepDst = Mat::zeros(src.size(), src.type());
	filter2D(src, noSepDst, -1, gauss2D);

	// ru�n� spo�t�te a nechte vypsat na v�stup - po�et operac� pro verzi s/bez vyu�it� separability 
	// sta�� zjednodu�en� v�po�et - po�et operac� nad pixely
	int pocet_operaci_separabilni_verze   = src.rows*src.cols*velikost*2;
	int pocet_operaci_neseparabilni_verze = src.rows*src.cols*velikost*velikost;
	
	std::cout << pocet_operaci_separabilni_verze << ";" << pocet_operaci_neseparabilni_verze << ";" << 1.*pocet_operaci_neseparabilni_verze/pocet_operaci_separabilni_verze << "x mene operaci";

	return;
}
