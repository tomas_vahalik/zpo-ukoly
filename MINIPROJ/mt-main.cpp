//---------------------------------------------------------------------------

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "mt-functions.h"

using namespace cv;
using namespace std;

#if defined _WIN32 || defined _WIN64
	string _DATA_PATH = "L:/ZPO/2012/data/";
#elif __linux__
	string _DATA_PATH = "./data/";
#endif

//#define DBG_SHOW(im1,im2,dif) {imshow("Test",im1);imshow("Ref",im2);imshow("Diff",dif);waitKey(0);}
//---------------------------------------------------------------------------
//
// Examples of input parameters
//
// mt-main.exe 1 2 3 ... - list of mini-tasks to run

void checkDifferences( const Mat& test, const Mat& ref, Mat&diff );

int main(int argc, char* argv[])
{
	// check input parameters
	if( argc <= 1 ) {
		cout << "No mini-tasks execution required." << endl;
		return 1;
	}

	// load testing images
	Mat Panorama = imread( _DATA_PATH + "panoramas.jpg" );

	// check testing images
	if( Panorama.empty() ) {
		cout << "Failed to load testing images." << endl;
		return 2;
	}

	// read input parameters
    for( int i = 1; i < argc; i++ ) {

		int fnc_id = atoi(argv[i]);

		// execute requested functions
		switch( fnc_id ) {

		/* **** Mini-�lohy - sada #1 ************************************* */
#ifdef MT01
		case 1: {

			Mat g1, g2, diff;
			
			// execute implemented function
			bgr2gray( Panorama, g1 );				
			
			// execute reference solution
			cvtColor( Panorama, g2, CV_BGR2GRAY );	

			// report error - (0.0, 1.0)
			cout << "MT #1;bgr2gray;";
			checkDifferences(g1,g2,diff);
			diff *= 255;
			imwrite( "01_bgr2gray.png", g1 );
			imwrite( "01_bgr2gray_err.png", diff );

			// execute implemented function
			histogramEqualization( g2, g1 );
			
			// execute reference solution
			equalizeHist( g2, g2 );

			// report error - (0.0, 1.0)
			cout << ";histogramEqualization;";
			checkDifferences(g1,g2,diff);
			//cout << endl;
			diff *= 255;
			imwrite( "01_he.png", g1 );
			imwrite( "01_he_err.png", diff );

			break;
		}
#endif
		/* *************************************************************** */

		/* **** Mini-�lohy - sada #2 ************************************* */
#ifdef MT02
		case 2: {
			Mat diff, gray;
			cvtColor( Panorama, gray, CV_BGR2GRAY );	

			// konvoluce
			float ker[9] = { -1, -2, -1, 0, 0, 0, 1, 2, 1 };
			Mat kernel( 3, 3, CV_32FC1, ker );
			Mat corr, corr_ref;// = Mat::zeros(gray.size(),gray.type());
			
			convolution( gray, kernel, corr );
			flip(kernel,kernel,-1);
			filter2D( gray, corr_ref, -1, kernel );
			
			// jeliko� blur funkce po��t� i hodnoty na okraj�ch v�stupn�ho obrazu (a my pro jednoduchost ne)
			// p�ed srovn�n�m vyma�eme krajn� hodnoty obrazu
			rectangle( corr_ref, Point(0,0), Point(corr_ref.cols-1,corr_ref.rows-1), Scalar::all(0), 1 );

			// report error - (0.0, 1.0)
			cout << "MT #2;convolution;"; 
			checkDifferences(corr,corr_ref,diff);
			diff *= 255;
			imwrite( "02_blurred.png", corr );
			imwrite( "02_blurred_err.png", diff );	
			//DBG_SHOW(corr,corr_ref,diff);

			// geometrick� transformace
			Mat T = Mat::eye( 3, 3, CV_32FC1 );		// translate
			T.at<float>(0,2) = -0.5f*gray.cols;				// translate in x
			T.at<float>(1,2) = -0.5f*gray.rows;				// translate in y

			Mat R = Mat::eye( 3, 3, CV_32FC1 );		// rotation by 0.05*PI angle
			R.at<float>(0,0) = (float)cos(0.05f*CV_PI);		
			R.at<float>(1,1) = R.at<float>(0,0);
			R.at<float>(0,1) = (float)sin(0.05f*CV_PI);				
			R.at<float>(1,0) = -R.at<float>(0,1);

			Mat S = Mat::eye( 3, 3, CV_32FC1 );		// scale
			S.at<float>(0,0) = 10.0f;				// scale in x
			S.at<float>(1,1) = 5.0f;				// scale in y

			Mat M = S*R*T;	// nejd��ve posuneme obr�zek tak, aby m�l st�ed v lev�m-horn�m rohu, pak orotujeme a nakonec zv�t��me 

			Mat transformed, transformed_ref;
			geometricalTransform( gray, transformed, M(Rect(0,0,3,2)) );
			warpAffine( gray, transformed_ref, M(Rect(0,0,3,2)), gray.size(), INTER_NEAREST, BORDER_CONSTANT, Scalar::all(0) );

			// report error - (0.0, 1.0)
			cout << ";geometricalTransform;";
			checkDifferences(transformed,transformed_ref,diff);
			//cout << endl;
			diff *= 255;
			imwrite( "02_transformed.png", transformed );
			imwrite( "02_transformed_err.png", diff );
			//DBG_SHOW(transformed,transformed_ref,diff);

			break;
		}
#endif
		/* *************************************************************** */

		/* **** Mini-�lohy - sada #3 ************************************* */
#ifdef MT03
		case 3: {
			Mat diff, gray;
			cvtColor( Panorama, gray, CV_BGR2GRAY );	
			
			cout << "MT #3 - DFT"; 

			Mat low_pass, high_pass;
			passFilter( gray, low_pass, 100, LOW_PASS_FILTER );
			passFilter( gray, high_pass, 100, HIGH_PASS_FILTER );

			Mat low_pass_ref, high_pass_ref, low_pass_ref_t, high_pass_ref_t;
			low_pass_ref_t  = imread( _DATA_PATH +"low_pass_ref.png");
			cvtColor( low_pass_ref_t, low_pass_ref, CV_BGR2GRAY );	
			high_pass_ref_t = imread( _DATA_PATH +"high_pass_ref.png");
			cvtColor( high_pass_ref_t, high_pass_ref, CV_BGR2GRAY );	

			cout << ";LowPass;";
			checkDifferences(low_pass,low_pass_ref,diff);
			diff *= 255;
			imwrite( "03_lowpass.png", low_pass );
			imwrite( "03_lowpass_err.png", diff );

			cout << ";HighPass;";
			checkDifferences(high_pass,high_pass_ref,diff);
			diff *= 255;
			imwrite( "03_highpass.png", high_pass );
			imwrite( "03_highpass_err.png", diff );

			break;
		}
#endif
		/* *************************************************************** */

		/* **** Mini-�lohy - sada #4 ************************************* */
#ifdef MT04
		case 4: {
			Mat diff, gray, zasum, sepOut, noSepOut, gauss_ref;
			cvtColor( Panorama, gray, CV_BGR2GRAY );	
			
			cout << "MT #4 - Separability;"; 

			bilySum( gray, zasum, 20.);//0.01 );
			cout << ";BilySum;";
			checkDifferences(gray,zasum,diff);
			diff *= 255;
			imwrite( "04_bilysum.png", zasum );
			imwrite( "04_bilysum_err.png", diff );
			
			// imshow( "BilySum", zasum );
			// imshow( "Err", diff );
			// waitKey();


			cout << ";GaussBlur;";
			separabilityGauss( zasum, 45, 8., sepOut, noSepOut );
			GaussianBlur( zasum, gauss_ref, Size(45,45), 8. );
			cout << ";";

			checkDifferences(sepOut,gauss_ref,diff);
			diff *= 255;
			imwrite( "04_sep.png", sepOut );
			imwrite( "04_nosep.png", noSepOut );
			imwrite( "04_sep_err.png", diff );

			//imshow( "Sep", sepOut );
			//imshow( "NoSep", noSepOut );
			//imshow( "Err", diff );
			//waitKey();

			break;
		}
#endif
		/* *************************************************************** */

		/* **** Mini-�lohy - sada #5 ************************************* */
#ifdef MT05
		case 5: {
			Mat diff, gray, zasum, medi, medi_ref;
			cvtColor( Panorama, gray, CV_BGR2GRAY );	
			
			cout << "MT #5 - Median;"; 

			int pocet = cvRound( 0.05*gray.cols*gray.rows);
			peprAsul( gray, zasum, pocet );
			cout << ";PeprASul;";
			checkDifferences(gray,zasum,diff);
			diff *= 255;
			imwrite( "05_peprasul.png", zasum );
			imwrite( "05_peprasul_err.png", diff );

			median( zasum, medi, 5 );
			medianBlur( zasum, medi_ref, 5 );

			cout << ";Median;";
			checkDifferences(medi,medi_ref,diff);
			diff *= 255;
			imwrite( "05_median.png", medi );
			imwrite( "05_median_err.png", diff );

			imshow( "PeprASul", zasum );
			imshow( "Median", medi );
			imshow( "Err", diff );
			waitKey();

			break;
		}
#endif
		/* *************************************************************** */

		default:
			break;
		}
	}

    return 0;
}
//---------------------------------------------------------------------------



void checkDifferences( const Mat& test, const Mat& ref, Mat& diff )
{
	absdiff( test, ref, diff );
	double mav;
	minMaxLoc( diff, NULL, &mav );
	double err = ( sum(diff).val[0] / (diff.rows*diff.cols) );
	double nonzeros = 1. * countNonZero( diff ) / (diff.rows*diff.cols);
	cout << err << ";" << mav << ";" << nonzeros;
	return;
}